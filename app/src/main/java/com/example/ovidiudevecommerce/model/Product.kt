package com.example.ovidiudevecommerce.model

data class Product(
    val title: String,
    val photoUrl: String,
    val price: String
) {
}